// SPDX-FileCopyrightText: 2019 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#define DEFAULT_INSTANCE "https://invidiou.site"

#define SETTINGS_INSTANCE "auth/instance"
#define SETTINGS_USERNAME "auth/username"
#define SETTINGS_COOKIE "auth/cookie"

#define INVIDIOUS_API_SEARCH "/api/v1/search"
#define INVIDIOUS_API_TRENDING "/api/v1/trending"
#define INVIDIOUS_API_TOP "/api/v1/top"
#define INVIDIOUS_API_VIDEOS_ "/api/v1/videos/"

#define INVIDIOUS_API_LOGIN "/login"
#define INVIDIOUS_API_SUBSCRIPTIONS "/api/v1/auth/subscriptions"
#define INVIDIOUS_API_SUBSCRIPTIONS_ "/api/v1/auth/subscriptions/"
#define INVIDIOUS_API_FEED "/api/v1/auth/feed"
